// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDFgPQngqUuLrAINej2BQBx6REXd1WZK38",
    authDomain: "angularclass-6416c.firebaseapp.com",
    databaseURL: "https://angularclass-6416c.firebaseio.com",
    projectId: "angularclass-6416c",
    storageBucket: "angularclass-6416c.appspot.com",
    messagingSenderId: "665831970631",
    appId: "1:665831970631:web:b6eaf03a25679f21a59e62",
    measurementId: "G-PH7J3C5923"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
