import { WeatherRaw } from './interfaces/weather-raw';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Weather } from './interfaces/weather';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TempService {

  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "44aa4ec13fdbe72d7c9897bf5e2f0f7c";
  private IMP = "&units=metric";

  searchWeatherdata(cityName:String):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
    .pipe(
      map(data => this.transformWatherData(data)),
      catchError(this.handleError)
    )
  }


  private handleError(res:HttpErrorResponse){
    console.log('In the service ' + res.error);
    return throwError(res.error)
  }


  private transformWatherData(data:WeatherRaw):Weather{
    return{
      name: data.name,
      country: data.sys.country,
      image: `http//api.openweathermap.org/img/w/${data.weather[0].icon}`,
      description: data.weather[0].description,
      temperature: data.main.temp,
      lat: data.coord.let,
      lon: data.coord.lon
    }
  }



  constructor(private http:HttpClient) { }
}


