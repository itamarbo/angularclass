import { Weather } from './../interfaces/weather';
import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { TempService } from './../temp.service';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  likes = 0;
  temperature; 
  city;
  image:String;
  errorMessage:String;
  hasError:Boolean = false;

  tempData$:Observable<Weather>;

  addLikes(){
    this.likes++
  }

  constructor(private route: ActivatedRoute, private tempService:TempService) { }

  ngOnInit() {
    //this.temperature = this.route.snapshot.params.temp;
    this.city= this.route.snapshot.params.city;
    this.tempData$ = this.tempService.searchWeatherdata(this.city);
    this.tempData$.subscribe(
      data => {
        console.log(data);
        this.temperature = data.temperature;
        this.image = data.image;
      },
      error =>{
        this.hasError = true;
        this.errorMessage = error.message;
        console.log('In a component ' + error.message);
      }
    )

  }

}
